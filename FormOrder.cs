using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DGVPrinterHelper;

namespace FinalProject
{
    public partial class FormOrder : Form
    {

        public FormOrder()
        {
            InitializeComponent();
        }

        private void FormOrder_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Cappucino")
            {
                textBox2.Text = "250000";

            }
            else if (comboBox1.Text == "Americano")
            {
                textBox2.Text = "200000";
            }

            else if (comboBox1.Text == "Ekspreso")
            {
                textBox2.Text = "150000";
            }
            else if (comboBox1.Text == "Latte")
            {
                textBox2.Text = "175000";
            }

           


        }

        private void button1_Click(object sender, EventArgs e)
        {
            double Price, Amount, hasil;
            Price = double.Parse(textBox2.Text);
            Amount = double.Parse(textBox3.Text);
            hasil = Price * Amount;
            textBox1.Text = hasil.ToString();
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            double Paid, hasil, Change, Price, Amount;
            Paid = double.Parse(textBox4.Text);
            Price = double.Parse(textBox2.Text);
            Amount = double.Parse(textBox3.Text);
            hasil = Price * Amount;
            Change = Paid - hasil;
            textBox5.Text = Change.ToString();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormMenu menu = new FormMenu();
            menu.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int baris = 0;
            dataGridView2.Rows.Add();
            baris = dataGridView2.Rows.Count - 2;

            //add dataGridView2
            dataGridView2[0, baris].Value = comboBox1.SelectedItem.ToString();
            dataGridView2[1, baris].Value = textBox2.Text;
            dataGridView2[2, baris].Value = dateTimePicker1.Text;
            dataGridView2[3, baris].Value = textBox3.Text;
            dataGridView2[4, baris].Value = textBox1.Text;
            dataGridView2[5, baris].Value = textBox4.Text;
            dataGridView2[6, baris].Value = textBox5.Text;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (this.dataGridView2.SelectedRows.Count > 0)
            {
                dataGridView2.Rows.RemoveAt(this.dataGridView2.SelectedRows[0].Index);
            };
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DGVPrinter printer = new DGVPrinter();
            printer.Title = "OSTERIA FRANCESCANA";
            printer.SubTitle = String.Format("Date {0}", DateTime.Now.Date.ToString("dd-MMMM-yyyy"));
            printer.SubTitleFormatFlags = StringFormatFlags.LineLimit | StringFormatFlags.NoClip;
            printer.PageNumbers = true;
            printer.PageNumberInHeader = false;
            printer.PorportionalColumns = true;
            printer.HeaderCellAlignment = StringAlignment.Near;
            printer.FooterSpacing = 15;
            printer.PrintPreviewDataGridView(dataGridView2);

        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormHome home = new FormHome();
            home.Show();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

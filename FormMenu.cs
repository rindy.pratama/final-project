using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace FinalProject
{
    public partial class FormMenu : Form
    {
        int PW;
        bool Hided;

        SqlConnection koneksi = new SqlConnection(@"Data Source=DESKTOP-SNJCIKD\SQLEXPRESS;Initial Catalog=Restaurant;Integrated Security=True");
        public FormMenu()
        {
            InitializeComponent();
            PW = Spanel2.Width;
            Hided = false;
        }

        private void FormMenu_Load(object sender, EventArgs e)
        {
            koneksi.Open();
            SqlDataAdapter dtap = new SqlDataAdapter("Select * from Menus", koneksi);
            DataTable dt = new DataTable();
            dtap.Fill(dt);
            dataGridView1.DataSource = dt;
            koneksi.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormHome home = new FormHome();
            home.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Hided) button2.Text = "HIDE";
            else button2.Text = "SHOW";
            timermenu.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Hided)
            {
                Spanel2.Width = Spanel2.Width + 15;
                if (Spanel2.Width >= PW)
                {
                    timermenu.Stop();
                    Hided = false;
                    this.Refresh();
                }

            } else {

                Spanel2.Width = Spanel2.Width - 15;
                if (Spanel2.Width <= 0)
                {
                    timermenu.Stop();
                    Hided = true;
                    this.Refresh();
                }
            }
            
        }

        private void Spanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormOrder order = new FormOrder();
            order.Show();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormCappucino cappucino = new FormCappucino();
            cappucino.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormEkspreso ekspreso = new FormEkspreso();
            ekspreso.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormAmericano americano = new FormAmericano();
            americano.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormLatte latte = new FormLatte();
            latte.Show();
        }
    }
}

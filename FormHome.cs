using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalProject
{
    public partial class FormHome : Form
    {
        int PW;
        bool Hided;

        public FormHome()
        {
            InitializeComponent();
            PW = Spanel.Width;
            Hided = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormMenu menu = new FormMenu();
            menu.Show();
        }

        private void Home_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormLogin login = new FormLogin();
            login.Show();

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Hided) button3.Text = "HIDE";
            else button3.Text = "SHOW";
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Hided) {
                Spanel.Width = Spanel.Width + 15;
                if(Spanel.Width >= PW)
                {
                    timer1.Stop();
                    Hided = false;
                    this.Refresh();
                }

            } else {
                Spanel.Width = Spanel.Width - 15;
                if(Spanel.Width <= 0)
                {
                    timer1.Stop();
                    Hided = true;
                    this.Refresh();
                }
            
            }
        }

        private void Spanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Waiting For Me :)", "MessageBox", MessageBoxButtons.YesNo, MessageBoxIcon.Warning); 
                
                    
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Make your order wkwk", "MessageBox", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
